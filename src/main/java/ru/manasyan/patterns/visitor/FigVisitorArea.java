package ru.manasyan.patterns.visitor;

import ru.manasyan.patterns.visitor.figure.Circle;
import ru.manasyan.patterns.visitor.figure.Rectangle;
import ru.manasyan.patterns.visitor.figure.Triangle;

public class FigVisitorArea extends FigVisitor {
    @Override
    public void visit(Rectangle figure) {
        float area = Math.abs(0.5f * (figure.a.getX() * figure.b.getY() +
                figure.b.getX() * figure.c.getY() +
                figure.c.getX() * figure.d.getY() +
                figure.d.getX() * figure.a.getY() -
                figure.b.getX() * figure.a.getY() -
                figure.c.getX() * figure.b.getY() -
                figure.d.getX() * figure.c.getY() -
                figure.a.getX() * figure.d.getY()));
        System.out.println("Rectangle Area: " + area);
    }

    @Override
    public void visit(Triangle figure) {
        float area = Math.abs(0.5f * (figure.a.getX() * figure.b.getY() +
                figure.b.getX() * figure.c.getY() +
                figure.c.getX() * figure.a.getY() -
                figure.b.getX() * figure.a.getY() -
                figure.c.getX() * figure.b.getY() -
                figure.a.getX() * figure.c.getY()));
        System.out.println("Triangle Area: " + area);
    }

    @Override
    public void visit(Circle figure) {
        System.out.println("Circle Area: " + Math.PI * Math.pow(figure.radius, 2));
    }
}

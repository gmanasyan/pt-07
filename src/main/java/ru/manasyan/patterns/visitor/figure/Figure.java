package ru.manasyan.patterns.visitor.figure;

import ru.manasyan.patterns.visitor.FigVisitor;

public abstract class Figure {

    private String name;

    public Figure(String name) {
        this.name = name;
    }

    public abstract void accept(FigVisitor visitor);

    public String getName() {
        return name;
    }
}

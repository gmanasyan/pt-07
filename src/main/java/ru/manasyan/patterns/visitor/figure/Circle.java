package ru.manasyan.patterns.visitor.figure;

import ru.manasyan.patterns.visitor.FigVisitor;

public class Circle extends Figure {

    public Dot center;
    public float radius;

    public Circle(Dot center, float radius) {
        super("Circle");
        this.center = center;
        this.radius = radius;
    }

    @Override
    public void accept(FigVisitor visitor) {
        visitor.visit(this);
    }
}

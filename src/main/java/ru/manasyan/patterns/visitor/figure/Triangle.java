package ru.manasyan.patterns.visitor.figure;

import ru.manasyan.patterns.visitor.FigVisitor;

public class Triangle extends Figure {

    public Dot a;
    public Dot b;
    public Dot c;

    public Triangle(Dot a, Dot b, Dot c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public void accept(FigVisitor visitor) {
        visitor.visit(this);
    }
}

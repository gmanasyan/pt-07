package ru.manasyan.patterns.visitor.figure;

import ru.manasyan.patterns.visitor.FigVisitor;

public class Rectangle extends Figure {

    public Dot a;
    public Dot b;
    public Dot c;
    public Dot d;

    public Rectangle(Dot a, Dot b, Dot c, Dot d) {
        super("Box");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public void accept(FigVisitor visitor) {
        visitor.visit(this);
    }
}

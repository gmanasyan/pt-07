package ru.manasyan.patterns.visitor;

import ru.manasyan.patterns.visitor.figure.Circle;
import ru.manasyan.patterns.visitor.figure.Dot;
import ru.manasyan.patterns.visitor.figure.Rectangle;
import ru.manasyan.patterns.visitor.figure.Triangle;

public class FigVisitorMove extends FigVisitor {

    private float x;

    private float y;

    public FigVisitorMove(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void visit(Rectangle figure) {
        move(figure.a);
        move(figure.b);
        move(figure.c);
        move(figure.d);
        System.out.printf("Move Rectangle x = %.2f, y = %.2f \n", x, y);
    }

    @Override
    public void visit(Triangle figure) {
        move(figure.a);
        move(figure.b);
        move(figure.c);
        System.out.printf("Move Triangle x = %.2f, y = %.2f \n", x, y);
    }

    @Override
    public void visit(Circle figure) {
        move(figure.center);
        System.out.printf("Move Circle x = %.2f, y = %.2f \n", x, y);
    }

    private void move(Dot a) {
        a.setX(a.getX() + x);
        a.setY(a.getY() + y);
    }

}

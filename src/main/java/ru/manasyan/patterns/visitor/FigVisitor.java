package ru.manasyan.patterns.visitor;

import ru.manasyan.patterns.visitor.figure.Circle;
import ru.manasyan.patterns.visitor.figure.Rectangle;
import ru.manasyan.patterns.visitor.figure.Triangle;

public abstract class FigVisitor {

    public abstract void visit(Rectangle figure);

    public abstract void visit(Triangle figure);

    public abstract void visit(Circle figure);


}

package ru.manasyan.patterns.visitor;

import ru.manasyan.patterns.visitor.figure.Circle;
import ru.manasyan.patterns.visitor.figure.Rectangle;
import ru.manasyan.patterns.visitor.figure.Triangle;

public class FigVisitorDraw extends FigVisitor {
    @Override
    public void visit(Rectangle figure) {
        System.out.println("Draw Rectangle");
    }

    @Override
    public void visit(Triangle figure) {
        System.out.println("Draw Triangle");
    }

    @Override
    public void visit(Circle figure) {
        System.out.println("Draw Circle");
    }
}

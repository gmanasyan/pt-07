package ru.manasyan.patterns.visitor;

import ru.manasyan.patterns.visitor.figure.*;

public class Main {

    public static void main(String[] args) {
        Figure[] figures = {new Circle(new Dot(0.0f,0.0f), 10.0f),
                new Rectangle(new Dot(0.0f,0.0f), new Dot(0.0f,3.0f),
                        new Dot(5.0f,3.0f), new Dot(5.0f,0.0f)),
                new Triangle(new Dot(0.0f,0.0f), new Dot(10.0f,0.0f),
                        new Dot(5.0f,10.0f))};

        FigVisitorArea visitorArea = new FigVisitorArea();
        FigVisitorDraw visitorDraw = new FigVisitorDraw();
        FigVisitorPerimeter visitorPerimeter = new FigVisitorPerimeter();
        FigVisitorMove visitorMove = new FigVisitorMove(10, 5);

        for (Figure figure : figures) {
            System.out.println("=====" + figure.getName() + "=====");
            figure.accept(visitorArea);
            figure.accept(visitorDraw);
            figure.accept(visitorPerimeter);
            figure.accept(visitorMove);
        }

    }
}

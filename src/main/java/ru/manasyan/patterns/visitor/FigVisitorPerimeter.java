package ru.manasyan.patterns.visitor;

import ru.manasyan.patterns.visitor.figure.Circle;
import ru.manasyan.patterns.visitor.figure.Rectangle;
import ru.manasyan.patterns.visitor.figure.Triangle;

public class FigVisitorPerimeter extends FigVisitor {
    @Override
    public void visit(Rectangle figure) {
        double sideAB = Math.sqrt(Math.pow(figure.a.getX() - figure.b.getX(), 2) +
                Math.pow(figure.a.getY() - figure.b.getY(), 2));
        double sideBC = Math.sqrt(Math.pow(figure.b.getX() - figure.c.getX(), 2) +
                Math.pow(figure.b.getY() - figure.c.getY(), 2));
        double sideCD = Math.sqrt(Math.pow(figure.c.getX() - figure.d.getX(), 2) +
                Math.pow(figure.c.getY() - figure.d.getY(), 2));
        double sideDA = Math.sqrt(Math.pow(figure.d.getX() - figure.a.getX(), 2) +
                Math.pow(figure.d.getY() - figure.a.getY(), 2));
        double perimeter = sideAB + sideBC + sideCD + sideDA;
        System.out.println("Rectangle Perimeter: " + perimeter);
    }

    @Override
    public void visit(Triangle figure) {
        double sideAB = Math.sqrt(Math.pow(figure.a.getX() - figure.b.getX(), 2) +
                Math.pow(figure.a.getY() - figure.b.getY(), 2));
        double sideBC = Math.sqrt(Math.pow(figure.b.getX() - figure.c.getX(), 2) +
                Math.pow(figure.b.getY() - figure.c.getY(), 2));
        double sideCA = Math.sqrt(Math.pow(figure.c.getX() - figure.a.getX(), 2) +
                Math.pow(figure.c.getY() - figure.a.getY(), 2));
        double perimeter = sideAB + sideBC + sideCA;
        System.out.println("Triangle Perimeter: " + perimeter);
    }

    @Override
    public void visit(Circle figure) {
        System.out.println("Circle Perimeter: " + 2 * Math.PI * figure.radius);
    }
}
